micloudeureka
=============

MI Cloud Service Discovery using Netflix's Eureka

---
TO MAKE THINGS EASIER I ADDED A START UP SCRIPT THAT CAN BE USED FOR DEVELOPMENT.
You can run the ```./start.sh``` script and if you want to debug use ```./start.sh debug```.

IF YOU WANT TO DEBUG:
    In Intellij open the toolbar and select Run --> Edit Configurations 
    Then you will want to add a new configuration, select + --> Remote
    In the new Remote window you can name the configuration something useful
    Next under Settings, set Transport: Socket, Debugger Mode: Attach, Host: localhost, Port: 5006 (port the start.sh script uses)

---

Run this project as a Spring Boot app (e.g. import into IDE and run
main method, or use "mvn spring-boot:run"). It will start up on port
8761 and serve the Eureka API from "/eureka".

These are the environment variables that need to be present on boot.  The required variables 
are needed so that we can access the config server to get our configuration file. Because of this
we cannot put plaintext data in the bootstrap.yml file as it will never decrypt fields, only the files
passed back from the config server will have the enctyped fields decrypted. 
    
    # REQUIRED
    export CONFIG_SERVER_USER=user
    export CONFIG_SERVER_PASS=mindsignited
    
    # OPTIONAL
    #points to given url by default
    export CONFIG_SERVER_URL=http://localhost:8888  
    # this is the password clients will use to access eureka service, you can also use {cipher} with an encrypted string in 
    # the micloudeureka.yml file - which is the default. 
    export EUREKA_SERVER_PASS=mindsignited 


This would be the command you can run to get this going from the commandline.

    CONFIG_SERVER_USER=user CONFIG_SERVER_PASS=mindsignited mvn spring-boot:run

## Resources

| Path             | Description  |
|------------------|--------------|
| /                        | Home page (HTML UI) listing service registrations          |
| /eureka/apps         | Raw registration metadata |



NOTE: One thing to keep in mind is the fact that we need to register eureka peers with each other.  We can use profiles 
for this but it needs to come from the micloudeureka.yml file in the config repository.  We need to do some exploration
into how we can manage this as our needs grow.  Do we want to look into the zookeeper/config server integration? will that
 make any difference? 
 
 
ALSO: it seems we have a NULL POINTER when a new service/node registers with Eureka.  It may only happen when we are in
peer awareness mode but that is what we need in production.  If you give Eureka some time, with the other service down,
it will recover from the race condition and be able to increment the service count. At this time you can safely start up
the service again and have it work. :(  This is what to look for in the Eureka logs:

