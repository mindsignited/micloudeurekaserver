#!/bin/bash

## MI Cloud Eureka Server

export SERVER_PORT=8761

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5006" --debug
else
    mvn spring-boot:run $@
fi
